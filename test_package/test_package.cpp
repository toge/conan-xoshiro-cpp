#include <iostream>

#include "XoshiroCpp.hpp"

int main() {
    auto rng = XoshiroCpp::Xoshiro256PlusPlus {12345};

    for (auto index = 0; index < 10; ++index) {
        auto val = rng();
        std::cout << val << std::endl;
    }

    return 0;
}
